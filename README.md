# Frontend

Ce projet est la partie front-end de l'application 'sheets'.

## Installation

Pour installer l'application, il faut d'abord npm pour récupérer les dépendances et NG pour lancer l'application.

* Installez (npm)[https://www.npmjs.com/] ;
* Avec npm, installez ng avec la commande suivante : `npm install -g @angular/cli` ;
* Installez les dépendances de l'application avec npm : `npm install` ;
* Lancez l'application avec la commande `ng serve`;
* L'application est maintenant utilisable sur un navigateur à l'adresse `http://localhost:4200/`.

## Utilisation

1. Se connecter à Google ;
2. Autorisez l'application Sheets à lire vos fichiers Spreadsheet ;
3. Mettez l'identifiant de la feuille Google Spreadsheet (l'identifiant se situe dans l'URL de la feuille SpreadSheet) ;
4. Cliquez ensuite sur le bouton pour trouver la feuille (s'il n'y a pas de résultat, l'identifiant est erroné ou n'existe pas) ;
5. Cliquez sur le bouton pour récupérer le contenu de la feuille.

Ensuite, vous pouvez utiliser l'application intuitivement.

## Déploiement

L'application est automatiquement déployée sur la branche `master` à l'adresse suivante [https://sheets.mathisfaiv.re](https://sheets.mathisfaiv.re).
Si vous souhaitez déployer l'application vous-même, il faut utiliser la commande `ng build`, qui va générer un dossier 'dist'. L'application se trouve dans ce dossier, il ne vous reste qu'à servir l'application depuis un serveur web comme Apache ou nginx par exemple.
