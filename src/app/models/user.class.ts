export class User {
    public kind: string;
      public displayName: string;
      public photoLink: string;
      public me: boolean;
      public permissionId: string;
      public emailAddress: string;
}
