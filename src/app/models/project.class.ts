import { GoogleFile } from "./google-file.class";
import { SheetContent } from "./sheet-content.class";

export class Project {
  public content: SheetContent;
  public metadata: GoogleFile;
}
