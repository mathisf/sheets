export class SpreadsheetProperties {
    title: string;
    locale: string;
    autoRecalc: string;
    timeZone: string;
    defaultFormat: any;
}

export class Sheet {
    properties: SheetProperties;
}

export class SheetProperties {
    sheetId: number;
    title: string;
    index: number;
    sheetType: string;
    gridProperties: any;
}

export class SpreadsheetMetadata {
    spreadsheetId: string;
    properties: SpreadsheetProperties;
    sheets: Array<Sheet>;
    spreadsheetUrl: string;
}

